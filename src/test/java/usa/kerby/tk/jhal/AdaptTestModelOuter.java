package usa.kerby.tk.jhal;

import usa.kerby.tk.jhateoas.DataTransferObject;

/**
 * @author Trevor Kerby
 * @since Jul 6, 2021
 */
public class AdaptTestModelOuter implements DataTransferObject {

	public AdaptTestModelInner inner;

	public AdaptTestModelOuter() {
		this.inner = new AdaptTestModelInner();
	}
}
