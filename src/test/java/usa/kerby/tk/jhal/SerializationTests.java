package usa.kerby.tk.jhal;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;
import usa.kerby.tk.jhateoas.DataTransferObject;
import usa.kerby.tk.jhateoas.Pagination;
import usa.kerby.tk.jhateoas.http.Hyperlink;
import usa.kerby.tk.jhateoas.http.RegisteredLinkRelation;

/**
 * @author Trevor Kerby
 * @since Jan 30, 2021
 */
public class SerializationTests {

	@Test
	public void TestSimple() {
		JsonbConfig config = new JsonbConfig().withFormatting(false);
		Jsonb jsonb = JsonbBuilder.create(config);
		Hal halo = new HalEntity.Builder(URI.create("myawesomesite.com"),
				(DataTransferObject) new TestModel("test-value"));
		String s = jsonb.toJson(halo.build());
		assertEquals(
				"{\"a\":\"test-value\",\"b\":{\"test\":\"value\"},\"_links\":{\"self\":{\"href\":\"myawesomesite.com\"}}}",
				s);
	}

	@Test
	public void TestLinkGroup() {
		JsonbConfig config = new JsonbConfig().withFormatting(false);
		Jsonb jsonb = JsonbBuilder.create(config);
		Hal halo = new HalEntity.Builder(URI.create("myawesomesite.com"),
				(DataTransferObject) new TestModel("test-value"));
		var hyperlinks = new ArrayList<Hyperlink>();
		hyperlinks.add(new Hyperlink("a"));
		hyperlinks.add(new Hyperlink("b"));
		halo.addLink(RegisteredLinkRelation.ABOUT, hyperlinks);
		String s = jsonb.toJson(halo.build());
		assertEquals(
				"{\"a\":\"test-value\",\"b\":{\"test\":\"value\"},\"_links\":{\"self\":{\"href\":\"myawesomesite.com\"},\"about\":[{\"href\":\"a\"},{\"href\":\"b\"}]}}",
				s);
	}

	@Test
	public void TestPaginated() {
		JsonbConfig config = new JsonbConfig().withFormatting(false);
		Jsonb jsonb = JsonbBuilder.create(config);

		List<TestModel> rels = new ArrayList<TestModel>();
		for (int i = 0; i < 3; i++) {
			rels.add(new TestModel("test-value" + i));
		}

		Pagination page = new Pagination((int) rels.size(), 0, rels.size());
		Hal pagination = new HalPagination.Builder(URI.create("myawesomesite.com"), page);
		for (int i = 0; i < rels.size(); i++) {
			TestModel rel = rels.get(i);
			pagination.addEmbedded(RegisteredLinkRelation.ITEM, URI.create("myawesomesite.com/" + rel.a), rel);
		}
		String s = jsonb.toJson(pagination.build());
		System.out.println(s);
		assertEquals(
				"{\"size\":3,\"offset\":0,\"limit\":3,\"_links\":{\"self\":{\"href\":\"myawesomesite.com\"}},\"_embedded\":{\"item\":[{\"a\":\"test-value0\",\"b\":{\"test\":\"value\"},\"_links\":{\"self\":{\"href\":\"myawesomesite.com/test-value0\"}}},{\"a\":\"test-value1\",\"b\":{\"test\":\"value\"},\"_links\":{\"self\":{\"href\":\"myawesomesite.com/test-value1\"}}},{\"a\":\"test-value2\",\"b\":{\"test\":\"value\"},\"_links\":{\"self\":{\"href\":\"myawesomesite.com/test-value2\"}}}]}}",
				s);
	}

	// @Test This doesn't work and won't be supported as of now
//	public void TestWithAdapter() {
//		JsonbConfig config = new JsonbConfig().withFormatting(false);
//		Jsonb jsonb = JsonbBuilder.create(config);
//		AdaptTestModelOuter outer = new AdaptTestModelOuter();
//		Hal halo = new HalEntity.Builder(URI.create("myawesomesite.com"), (DataTransferObject) outer);
//		String s = jsonb.toJson(halo.build());
//	}

}
