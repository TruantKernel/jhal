package usa.kerby.tk.jhal;

import jakarta.json.bind.adapter.JsonbAdapter;

/**
 * @author Trevor Kerby
 * @since Jul 6, 2021
 */
public class AdaptTestModelAdapter implements JsonbAdapter<AdaptTestModelInner, String> {

	@Override
	public String adaptToJson(AdaptTestModelInner obj) throws Exception {
		System.out.println("AdaptTestModelAdapter::adaptToJson");
		return "Nothing to do, really";
	}

	@Override
	public AdaptTestModelInner adaptFromJson(String obj) throws Exception {
		System.out.println("AdaptTestModelAdapter::adaptFromJson");
		return new AdaptTestModelInner();
	}

}
