package usa.kerby.tk.jhal;

import java.util.HashMap;
import java.util.Map;

import usa.kerby.tk.jhateoas.DataTransferObject;

public class TestModel implements DataTransferObject{
	public String a;
	public Map<String, String> b;
	/**
	 * 
	 */
	public TestModel(String a) {
		this.a = a;
		this.b = new HashMap<>();
		this.b.put("test", "value");
	}
}