package usa.kerby.tk.jhal;

import jakarta.json.bind.annotation.JsonbTypeAdapter;

/**
 * @author Trevor Kerby
 * @since Jul 6, 2021
 */
@JsonbTypeAdapter(AdaptTestModelAdapter.class)
public class AdaptTestModelInner {

}
