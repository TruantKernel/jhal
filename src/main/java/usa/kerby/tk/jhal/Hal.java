package usa.kerby.tk.jhal;

import java.net.URI;
import java.util.List;
import java.util.Map;

import com.jayway.jsonpath.JsonPath;

import jakarta.ws.rs.core.MediaType;
import usa.kerby.tk.jhal.jsonpath.PathDecorator;
import usa.kerby.tk.jhateoas.DataTransferObject;
import usa.kerby.tk.jhateoas.Hateoas;
import usa.kerby.tk.jhateoas.http.Hyperlink;
import usa.kerby.tk.jhateoas.http.LinkRelation;

/**
 * @author Trevor Kerby
 * @since Aug 6, 2020
 */
public interface Hal extends Hateoas {

	public static final String APPLICATION_JSON_HAL = "application/hal+json";
	public static final MediaType APPLICATION_JSON_HAL_TYPE = new MediaType("application", "hal+json");

	public static final String APPLICATION_XML_HAL = "application/hal+xml";
	public static final MediaType APPLICATION_XML_HAL_TYPE = new MediaType("application", "hal+xml");

	public Hal addEmbedded(LinkRelation rel, Hal builder);

	public Hal addEmbedded(LinkRelation rel, URI self, DataTransferObject... dto);

	public JsonPath addLink(LinkRelation rel, Hyperlink link);

	public List<JsonPath> addLink(LinkRelation rel, List<Hyperlink> links);

	public void registerCuries(List<Hyperlink> links);

	public HalRepresentation build();

	public List<Hal> getEmbedded(LinkRelation rel);

	public Hal getEmbedded(LinkRelation rel, int index);

	public String getPath();

	/**
	 * @return
	 */
	public Map<LinkRelation, List<PathDecorator<Hyperlink>>> getLinks();

	/**
	 * @return
	 */
	public Map<LinkRelation, List<Hal>> getEmbedded();

	/**
	 * @return
	 */
	public Map<String, PathDecorator<Object>> getProps();
}
