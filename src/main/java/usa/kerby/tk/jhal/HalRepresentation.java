package usa.kerby.tk.jhal;

import java.util.List;
import java.util.Map;

import usa.kerby.tk.jhal.jsonpath.PathDecorator;
import usa.kerby.tk.jhateoas.HateoasRepresentation;
import usa.kerby.tk.jhateoas.http.Hyperlink;
import usa.kerby.tk.jhateoas.http.LinkRelation;

/**
 * @author Trevor Kerby
 * @since Aug 6, 2020
 */
public interface HalRepresentation extends HateoasRepresentation {

	/**
	 * @return
	 */
	Map<String, PathDecorator<Object>> getProps();

	/**
	 * @return
	 */
	Map<LinkRelation, List<PathDecorator<Hyperlink>>> getLinks();

	/**
	 * @return
	 */
	Map<LinkRelation, List<HalRepresentation>> getEmbedded();

}
