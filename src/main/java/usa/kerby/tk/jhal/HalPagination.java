package usa.kerby.tk.jhal;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;

import jakarta.json.bind.annotation.JsonbTransient;
import usa.kerby.tk.jhal.jsonpath.PathDecorator;
import usa.kerby.tk.jhateoas.DataTransferObject;
import usa.kerby.tk.jhateoas.Pagination;
import usa.kerby.tk.jhateoas.http.Hyperlink;
import usa.kerby.tk.jhateoas.http.LinkRelation;
import usa.kerby.tk.jhateoas.http.RegisteredLinkRelation;
import usa.kerby.tk.pageable.Pageable;

/**
 * @author Trevor Kerby
 * @since Jun 24, 2020
 */
public class HalPagination extends HalEntity implements Pageable {

	public static class Builder extends HalEntity.Builder implements Pageable {
		private int offset = Integer.valueOf(Pageable.DEFAULT_OFFSET);
		private int limit = Integer.valueOf(Pageable.DEFAULT_LIMIT);
		private int size = 0;

		public Builder(URI uri, int size, int offset, int limit) {
			super(uri, (DataTransferObject) null);
			this.uri = uri;
			this.offset = offset;
			this.size = size;
			this.limit = limit;
			this.registerProps();
		}

		public Builder(URI uri, Pagination page) {
			this(uri, page.size, page.offset, page.limit);
		}

		@Override
		public HalPagination build() {
			return new HalPagination(this);
		}

		public void createPageLinks() {
			if (this.continuesBackward()) {
				this.addLink(RegisteredLinkRelation.FIRST,
						Hyperlink.createPaginationHyperlink(this.uri, firstPage(), this.limit));
				this.addLink(RegisteredLinkRelation.PREV,
						Hyperlink.createPaginationHyperlink(this.uri, lastPage(), this.limit));
			}
			if (this.continuesForward()) {
				this.addLink(RegisteredLinkRelation.NEXT,
						Hyperlink.createPaginationHyperlink(this.uri, nextPage(), this.limit));
				this.addLink(RegisteredLinkRelation.LAST,
						Hyperlink.createPaginationHyperlink(this.uri, lastPage(), this.limit));
			}
		}

		@Override
		public int getLimit() {
			return this.limit;
		}

		@Override
		public int getOffset() {
			return this.offset;
		}

		@Override
		public int getSize() {
			return this.size;
		}

		@Override
		@NonProp
		public HalPagination.Builder getEmbedded(LinkRelation rel, int index) {
			return (HalPagination.Builder) embedded.get(rel).get(index);
		}

		public Builder limit(int limit) {
			this.limit = limit;
			return this;
		}

		public Builder offset(int offset) {
			this.offset = offset;
			return this;
		}

		public Builder pagination(Pagination page) {
			this.offset = page.offset;
			this.limit = page.limit;
			this.size = page.size;
			return this;
		}

		@Override
		protected Builder self() {
			return this;
		}

		private void registerProps() {
			for (Method m : this.getClass().getDeclaredMethods()) {
				if (m.getName().startsWith("get") && !m.isAnnotationPresent(JsonbTransient.class)
						&& !m.isAnnotationPresent(NonProp.class)) {
					try {
						String name = m.getName().substring(3).toLowerCase();
						this.props.put(name, new PathDecorator<Object>(m.invoke(this), this.getPath() + "." + name));
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						new IllegalArgumentException("Attempted to call " + m.getName() + " with no arguements", e);
					}
				}
			}
		}

		public Builder size(int size) {
			this.size = size;
			return this;
		}
	}

	private final Integer offset;
	private final Integer limit;

	private final Integer size;

	public HalPagination(Builder builder) {
		super(builder);
		this.size = builder.size;
		this.offset = builder.offset;
		this.limit = builder.limit;
	}

	@Override
	public int getLimit() {
		return this.limit;
	}

	@Override
	public int getOffset() {
		return this.offset;
	}

	@Override
	public int getSize() {
		return this.size;
	}

}
