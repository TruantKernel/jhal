package usa.kerby.tk.jhal.curie;

import usa.kerby.tk.jhateoas.http.LinkRelation;

/**
 * @author Trevor Kerby
 * @since Feb 1, 2021
 */
public enum CurieLinkRelation implements LinkRelation {
	CURIES("curies");

	private final String name;

	/**
	 * @param string
	 */
	CurieLinkRelation(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String toString() {
		return this.name;
	}

	@Override
	public String getName() {
		return this.name;
	}

}