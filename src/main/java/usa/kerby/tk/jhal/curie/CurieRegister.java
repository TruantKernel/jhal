package usa.kerby.tk.jhal.curie;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import usa.kerby.tk.jhateoas.http.Hyperlink;

/**
 * @author Trevor Kerby
 * @since Aug 13, 2020
 */

public class CurieRegister {

	public static final String seperator = "/";

	private List<Hyperlink> curies;

	public CurieRegister() {
		this.curies = new ArrayList<Hyperlink>();
	}

	public void add(String name, String templateString) {
		try {
			this.curies.add(new Hyperlink.Builder(templateString).named(name).build());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	public void add(String name, URI uri) {
		this.curies.add(new Hyperlink.Builder(uri).named(name).build());
	}

	public void add(Hyperlink link) {
		this.curies.add(link);
	}

	/**
	 * @return the Hyperlinks
	 */
	public List<Hyperlink> getCuries() {
		return curies;
	}

}
