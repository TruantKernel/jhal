package usa.kerby.tk.jhal;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.jayway.jsonpath.JsonPath;

import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.json.bind.annotation.JsonbTypeSerializer;
import usa.kerby.tk.jhal.curie.CurieLinkRelation;
import usa.kerby.tk.jhal.jsonpath.PathDecorator;
import usa.kerby.tk.jhateoas.DataTransferObject;
import usa.kerby.tk.jhateoas.HateoasEntity;
import usa.kerby.tk.jhateoas.http.Hyperlink;
import usa.kerby.tk.jhateoas.http.LinkRelation;
import usa.kerby.tk.jhateoas.http.RegisteredLinkRelation;

/**
 * @author Trevor Kerby
 * @since Jun 18, 2020
 */

@JsonbTypeSerializer(HalSerializer.class)
public class HalEntity extends HateoasEntity implements HalRepresentation {
	public static class Builder extends HateoasEntity.Builder<Builder> implements Hal {

		protected String path;
		protected Map<LinkRelation, List<PathDecorator<Hyperlink>>> links;
		protected Map<LinkRelation, List<Hal>> embedded;
		protected Map<String, PathDecorator<Object>> props;

		/**
		 * @param self
		 * @param dto
		 */

		public Builder(URI self, DataTransferObject... dto) {
			super(self, dto);
			this.path = HalEntity.DEFAULT_PATH;
			this.links = new LinkedHashMap<LinkRelation, List<PathDecorator<Hyperlink>>>();
			this.embedded = new LinkedHashMap<LinkRelation, List<Hal>>();
			this.addLink(RegisteredLinkRelation.SELF, new Hyperlink.Builder(this.uri).build());
			this.props = this.registerDtoProps(dto);
		}

		@Override
		public HalEntity.Builder addEmbedded(LinkRelation rel, Hal builder) {
			if (!this.embedded.containsKey(rel)) {
				this.embedded.put(rel, new ArrayList<Hal>());
			}
			if (this.path != null && ((HalEntity.Builder) builder).getPath() == null) {
				((HalEntity.Builder) builder)
						.withPath(this.path + "." + rel + "[" + this.embedded.get(rel).size() + "]");
			}
			this.embedded.get(rel).add(builder);
			return (HalEntity.Builder) builder;
		}

		@Override
		public HalEntity.Builder addEmbedded(LinkRelation rel, URI self, DataTransferObject... dto) {
			return this.addEmbedded(rel, new HalEntity.Builder(self, dto));
		}

		@Override
		public JsonPath addLink(LinkRelation rel, Hyperlink link) {
			var linkGroup = this.getLinkGroup(rel);
			return this.addLinkToGroup(link, rel, linkGroup);
		}

		@Override
		public List<JsonPath> addLink(LinkRelation rel, List<Hyperlink> links) {
			List<JsonPath> paths = new ArrayList<JsonPath>(links.size());
			if (links != null) {
				var linkGroup = this.getLinkGroup(rel);
				for (Hyperlink link : links) {
					var path = this.addLinkToGroup(link, rel, linkGroup);
					paths.add(path);
				}
			}
			return paths;
		}

		@Override
		public void registerCuries(List<Hyperlink> curies) {
			if (!curies.isEmpty()) {
				this.addLink(CurieLinkRelation.CURIES, curies);
			}
		}

		@Override
		public HalRepresentation build() {
			return new HalEntity(this);
		}

		@Override
		public Map<LinkRelation, List<Hal>> getEmbedded() {
			return this.embedded;
		}

		@Override
		@NonProp
		public List<Hal> getEmbedded(LinkRelation rel) {
			return embedded.get(rel);
		}

		@Override
		@NonProp
		public Hal getEmbedded(LinkRelation rel, int index) {
			return embedded.get(rel).get(index);
		}

		@Override
		public Map<LinkRelation, List<PathDecorator<Hyperlink>>> getLinks() {
			return this.links;
		}

		/**
		 * @return the path
		 */

		@Override
		@NonProp
		public String getPath() {
			return this.path;
		}

		@Override
		public Map<String, PathDecorator<Object>> getProps() {
			return this.props;
		}

		@Override
		public URI getUri() {
			return this.uri;
		}

		private List<PathDecorator<Hyperlink>> getLinkGroup(LinkRelation rel) {
			var linkGroup = this.links.get(rel);
			if (linkGroup == null) {
				linkGroup = new ArrayList<PathDecorator<Hyperlink>>();
				this.links.put(rel, linkGroup);
			}
			return linkGroup;
		}

		private JsonPath addLinkToGroup(Hyperlink link, LinkRelation rel, List<PathDecorator<Hyperlink>> linkGroup) {
			PathDecorator<Hyperlink> decorator = new PathDecorator<Hyperlink>(link,
					this.path + "." + HalEntity.LINKS_PROPERTY_NAME + "." + rel + "[" + linkGroup.size() + "]"
							+ ".href");
			linkGroup.add(decorator);
			return decorator.getPath();
		}

		private void reconfigurePaths() {
			if (this.props != null) {
				for (Map.Entry<String, PathDecorator<Object>> entry : this.props.entrySet()) {
					this.props.replace(entry.getKey(), entry.getValue(),
							new PathDecorator<Object>(entry.getValue().getValue(), this.path));
				}
			}
		}

		private Map<String, PathDecorator<Object>> registerDtoProps(DataTransferObject[] transferObjects) {
			Map<String, PathDecorator<Object>> map = new HashMap<String, PathDecorator<Object>>();
			for (DataTransferObject dto : transferObjects) {
				if (dto != null) {
					for (Field f : dto.getClass().getDeclaredFields()) {
						if (Modifier.isPublic(f.getModifiers())) {
							try {
								map.put(f.getName(),
										new PathDecorator<Object>(f.get(dto), this.getPath() + "." + f.getName()));
							} catch (IllegalArgumentException | IllegalAccessException e) {
								e.printStackTrace();
							}
						} else {
							String getterName = "get" + f.getName().substring(0, 1).toUpperCase()
									+ f.getName().substring(1);
							try {
								Object obj = dto.getClass().getMethod(getterName).invoke(dto);
								map.put(f.getName(),
										new PathDecorator<Object>(obj, this.getPath() + "." + f.getName()));
							} catch (NoSuchMethodException | SecurityException | IllegalAccessException
									| IllegalArgumentException | InvocationTargetException e) {
								System.out.println("failed to call " + getterName);
								e.printStackTrace();
							}
						}
					}
				}
			}
			return map;
		}

		public void removeEmbedded(LinkRelation rel, Hal halo) {
			List<Hal> halos = this.embedded.get(rel);
			if (halos != null) {
				halos.remove(halo);
			}
		}

		public void removeLink(LinkRelation rel) {
			this.links.remove(rel);
		}

		@Override
		protected Builder self() {
			return this;
		}

		public Builder withPath(String path) {
			this.path = path;
			this.reconfigurePaths();
			return this;
		}

	}

	protected static final String DEFAULT_PATH = "$";
	public static final String SELF_LINK_PATH_FRAGMENT = "_links.self.href";
	public static final String LINKS_PROPERTY_NAME = "_links";
	public static final String EMBEDDED_PROPERTY_NAME = "_embedded";

	private static List<HalRepresentation> buildChildren(List<Hal> unbuilt) {
		if (unbuilt != null && !unbuilt.isEmpty()) {
			List<HalEntity> list = new ArrayList<HalEntity>(unbuilt.size());
			unbuilt.forEach((s) -> list.add((HalEntity) s.build()));
			return Collections.unmodifiableList(list);
		}
		return null;
	}

	private static Map<LinkRelation, List<HalRepresentation>> buildEmbedded(Map<LinkRelation, List<Hal>> embedded) {
		final Map<LinkRelation, List<HalRepresentation>> map = new LinkedHashMap<>();
		for (Map.Entry<LinkRelation, List<Hal>> entry : embedded.entrySet()) {
			map.put(entry.getKey(), HalEntity.buildChildren(entry.getValue()));
		}
		return Collections.unmodifiableMap(map);
	}

	private final String path;

	protected Map<LinkRelation, List<PathDecorator<Hyperlink>>> links;

	protected Map<LinkRelation, List<HalRepresentation>> embedded;

	protected Map<String, PathDecorator<Object>> props;

	protected HalEntity(Hal builder) {
		super(builder);
		this.path = (builder.getPath() == null) ? HalEntity.DEFAULT_PATH : builder.getPath();
		this.links = Collections.unmodifiableMap(builder.getLinks());
		this.embedded = HalEntity.buildEmbedded(builder.getEmbedded());
		this.props = Collections.unmodifiableMap(builder.getProps());
	}

	// @NonProp
	@Override
	@JsonbProperty("_embedded")
	public Map<LinkRelation, List<HalRepresentation>> getEmbedded() {
		return embedded;
	}

	@JsonbTransient
	public List<HalRepresentation> getEmbedded(LinkRelation rel) {
		return embedded.get(rel);
	}

	@JsonbTransient
	public HalRepresentation getEmbedded(LinkRelation rel, int index) {
		return embedded.get(rel).get(index);
	}

	@JsonbTransient
	public List<PathDecorator<Hyperlink>> getLink(LinkRelation rel) {
		return this.getLinks().get(rel);
	}

	// @NonProp
	@Override
	@JsonbProperty("_links")
	public Map<LinkRelation, List<PathDecorator<Hyperlink>>> getLinks() {
		return links;
	}

	@JsonbTransient
	public String getPath() {
		return path;
	}

	@JsonbTransient
	public PathDecorator<Object> getProp(String key) {
		return this.props.get(key);
	}

	@Override
	@JsonbTransient
	public Map<String, PathDecorator<Object>> getProps() {
		return props;
	}

}
