package usa.kerby.tk.jhal.jsonpath;

import com.jayway.jsonpath.JsonPath;

/**
 * @author Trevor Kerby
 * @since Jul 20, 2020
 */
public class PathDecorator<T> implements Pathable {
	private T wrapped;
	private JsonPath path;

	public PathDecorator(T wrapped, String path) {
		this(wrapped, JsonPath.compile(path));
	}
	
	public PathDecorator(T wrapped, JsonPath path) {
		this.wrapped = wrapped;
		this.path = path;
	}

	public T getValue() {
		return this.wrapped;
	}

	public JsonPath getPath() {
		return this.path;
	}

}
