package usa.kerby.tk.jhal;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import jakarta.json.bind.serializer.JsonbSerializer;
import jakarta.json.bind.serializer.SerializationContext;
import jakarta.json.stream.JsonGenerator;
import usa.kerby.tk.jhal.jsonpath.PathDecorator;
import usa.kerby.tk.jhateoas.http.Hyperlink;
import usa.kerby.tk.jhateoas.http.LinkRelation;

/**
 * @author Trevor Kerby
 * @since Jul 20, 2020
 */
public class HalSerializer implements JsonbSerializer<HalRepresentation> {

	@Override
	public void serialize(HalRepresentation child, JsonGenerator generator, SerializationContext ctx) {
		// generator.writeStartObject();
		this.writeProps(child.getProps(), generator, ctx);
		this.writeLinks(child.getLinks(), generator, ctx);
		this.writeEmbedded(child.getEmbedded(), generator, ctx);
		// generator.writeEnd();
	}

	private void writeEmbedded(Map<LinkRelation, List<HalRepresentation>> map, JsonGenerator generator,
			SerializationContext ctx) {
		if (map != null && !map.isEmpty()) {
			generator.writeStartObject("_embedded");
			for (Entry<LinkRelation, List<HalRepresentation>> entry : map.entrySet()) {
				generator.writeStartArray(entry.getKey().toString());
				for (HalRepresentation child : entry.getValue()) {
					generator.writeStartObject();
					this.serialize(child, generator, ctx);
					generator.writeEnd();
				}
				generator.writeEnd();
			}
			generator.writeEnd();
		}
	}

	/**
	 * 
	 */
	private void writeLinks(Map<LinkRelation, List<PathDecorator<Hyperlink>>> links, JsonGenerator generator,
			SerializationContext ctx) {
		if (links != null && !links.isEmpty()) {
			generator.writeStartObject("_links");
			for (Entry<LinkRelation, List<PathDecorator<Hyperlink>>> entry : links.entrySet()) {
				var linkGroup = entry.getValue();
				var linkCount = linkGroup.size();
				if (linkCount == 0) {
					continue;
				} else if (linkGroup.size() == 1) {
					PathDecorator<Hyperlink> linkItem = linkGroup.get(0);
					ctx.serialize(entry.getKey().toString(), linkItem.getValue(), generator);
				} else {
					generator.writeStartArray(entry.getKey().toString());
					for (PathDecorator<Hyperlink> linkItem : entry.getValue()) {
						generator.writeStartObject();
						ctx.serialize(linkItem.getValue(), generator);
					}
					// close object
					generator.writeEnd();
					// close array
					generator.writeEnd();
				}
			}
			generator.writeEnd();
		}
	}

	/**
	 * @param halo
	 * @param generator
	 * @param ctx
	 */
	private void writeProps(Map<String, PathDecorator<Object>> props, JsonGenerator generator,
			SerializationContext ctx) {
		if (props != null && !props.isEmpty()) {
			for (Entry<String, PathDecorator<Object>> entry : props.entrySet()) {
				PathDecorator<Object> pathedHyperlink = entry.getValue();
				ctx.serialize(entry.getKey(), pathedHyperlink.getValue(), generator);
			}
		}

	}
}
