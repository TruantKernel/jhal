# JHAL
`jhal` is a simple implementation of [JSON Hypertext Application Language](https://datatracker.ietf.org/doc/html/draft-kelly-json-hal), written in Java, for the purpose of producing JSON representations. It depends on [JHATEOAS](https://gitlab.com/073TruantKernel/jhateoas).

`jhal` allows `DataTransferObject`s to serialized, along with `_links` and `_embedded`, as a class that extends `HalEntity` and implements the `HalRepresentation` interface. The instantiation of a `HalRepresentation` is handled by a builder that implements the `Hal` interface and serialization is provided by `HalSerializer`. A paginated HAL representation, `HalPagination` is also included.

A simple `CurieRegister` is provided by this library to store into, and retrieve, a list of [CURIEs](https://www.w3.org/TR/curie/).

This library is utilized by its developer but has not been extensively tested in production. Please use with caution.

# Example Document Hal Representation

```java
public class TestModel implements DataTransferObject{
	private String a;

	public TestModel(String a) {
		this.a = a;
	}
	
	public getA(){
		return this.a;
	}
}
```

``` java
Hal halo = new HalEntity.Builder(URI.create("myawesomesite.com"),
				(DataTransferObject) new TestModel("test-value"));
String json = jsonb.toJson(halo.build());
```

# Example Collection Hal Representation

```java

List<TestModel> rels = new ArrayList<TestModel>();
for (int i = 0; i < 3; i++) {
	rels.add(new TestModel("test-value" + i));
}

Pagination page = new Pagination((int) rels.size(), 0, rels.size());
Hal pagination = new HalPagination.Builder(URI.create("myawesomesite.com"), page);
for (int i = 0; i < rels.size(); i++) {
	TestModel rel = rels.get(i);
	pagination.addEmbedded(RegisteredLinkRelation.ITEM, URI.create("myawesomesite.com/" + rel.a), rel);
}

String json = jsonb.toJson(pagination.build());

```
